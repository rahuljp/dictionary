package com.example.dictionary.API.Models

data class Definition(
    val definition: String,
    val example: String,
    val synonyms: List<String>
)