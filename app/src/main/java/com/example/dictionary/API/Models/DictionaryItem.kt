package com.example.dictionary.API.Models

data class DictionaryItem(
    val meanings: List<Meaning>,
    var word: String
)