package com.example.dictionary.API.Models

data class Meaning(
    val definitions: List<Definition>,
    val partOfSpeech: String
)