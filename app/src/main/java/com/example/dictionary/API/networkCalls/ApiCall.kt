package com.example.dictionary.API.networkCalls

import com.example.dictionary.API.Models.Dictionary
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiCall {
    @GET("/api/v2/entries/en/{word}")
    fun getData(@Path("word") word:String) : Call<Dictionary>

    companion object{
        operator fun invoke() : ApiCall{
            return Retrofit.Builder()
                .baseUrl("https://api.dictionaryapi.dev/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiCall :: class.java)
        }
    }
}