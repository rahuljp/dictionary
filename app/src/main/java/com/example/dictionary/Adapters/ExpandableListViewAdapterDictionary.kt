package com.example.dictionary.Adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import androidx.core.view.marginLeft
import com.example.dictionary.Databases.DictionaryDatabase
import com.example.dictionary.Databases.Entity.Dictionary
import com.example.dictionary.Databases.Helper.DBHelper
import com.example.dictionary.R
import kotlinx.android.synthetic.main.dictionary_list_items.view.*
import kotlinx.android.synthetic.main.offline_words_list.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ExpandableListViewAdapterDictionary(words: List<Dictionary>) : BaseExpandableListAdapter() {
    var words=words

    lateinit var context : Context
    override fun getGroupCount(): Int {
        return words.size
    }

    override fun getChildrenCount(p0: Int): Int {
        return words[p0].meanings.size
    }

    override fun getGroup(p0: Int): Any {
        return words[p0]
    }

    override fun getChild(p0: Int, p1: Int): Any {
        return words[p0].meanings[p1]
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p1.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        context=p3!!.context
        var view = LayoutInflater.from(context).inflate(R.layout.offline_words_list, p3, false)
        view.wordsss.text=words[p0].word
        view.imageButton.setOnClickListener {
            var word=words[p0]
            (words as ArrayList<Dictionary>).removeAt(p0)
            notifyDataSetChanged()
            GlobalScope.launch {
                DictionaryDatabase.getInstance(context).DictionaryDao().deleteword(word)
            }

        }
        return view
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        var data=words[p0].meanings[p1]
        var view=LayoutInflater.from(context).inflate(R.layout.dictionary_list_items,p4,false)
        view.pos.text=data.partOfSpeech
        view.df.text=data.definitions[0].definition
        view.ex.text=data.definitions[0].example
        view.sy.text=data.definitions[0].synonyms.toString()
        return view
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return true
    }


}
