package com.example.dictionary.Adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.dictionary.API.Models.Meaning
import com.example.dictionary.R
import kotlinx.android.synthetic.main.dictionary_list_items.view.*

class ListAdapterDictionary(list : ArrayList<Meaning>) : BaseAdapter() {
    val list=list
    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(p0: Int): Any {
        return p0
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        var view=LayoutInflater.from(p2?.context).inflate(R.layout.dictionary_list_items,p2,false)
        view.pos.setText(list[p0].partOfSpeech)
        view.df.setText(list[p0].definitions[0].definition)
        view.sy.setText(list[p0].definitions[0].synonyms.toString())
        view.ex.setText(list[0].definitions[0].example)
        return view
    }
}