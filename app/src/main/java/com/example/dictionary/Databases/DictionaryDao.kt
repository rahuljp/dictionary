package com.example.dictionary.Databases

import androidx.room.*
import com.example.dictionary.API.Models.Dictionary
import com.example.dictionary.API.Models.DictionaryItem

@Dao
interface DictionaryDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addWord(list : com.example.dictionary.Databases.Entity.Dictionary)

    @Query("SELECT * FROM DICTIONARY WHERE WORD=:word")
    fun getWord(word : String) : List<com.example.dictionary.Databases.Entity.Dictionary>

    @Query("SELECT * FROM DICTIONARY")
    fun allwords() : List<com.example.dictionary.Databases.Entity.Dictionary>

    @Delete
    fun deleteword(word : com.example.dictionary.Databases.Entity.Dictionary)

    @Query("DELETE  FROM DICTIONARY")
    fun deleteall()
}