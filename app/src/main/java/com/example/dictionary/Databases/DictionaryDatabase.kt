package com.example.dictionary.Databases

import android.content.Context
import androidx.room.*
import com.example.dictionary.API.Models.DictionaryItem
import com.example.dictionary.Databases.Entity.Dictionary
import com.example.dictionary.Databases.typeConverter.Meaningconverter

@Database(entities = [Dictionary::class], version = 1)
@TypeConverters(Meaningconverter::class)
abstract class DictionaryDatabase : RoomDatabase() {
    abstract fun DictionaryDao() : DictionaryDao


    companion object {
        var instance: DictionaryDatabase? = null
        fun getInstance(context: Context) : DictionaryDatabase {
            if (instance == null) {
                instance = Room.databaseBuilder(context, DictionaryDatabase::class.java, "dict.db")
                    .build()
            }
            return instance as DictionaryDatabase
        }
    }

}