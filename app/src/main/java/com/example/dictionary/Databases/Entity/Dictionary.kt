package com.example.dictionary.Databases.Entity

import androidx.room.*
import com.example.dictionary.API.Models.Meaning

@Entity
data class Dictionary(@PrimaryKey val word : String, val meanings: List<Meaning>)
