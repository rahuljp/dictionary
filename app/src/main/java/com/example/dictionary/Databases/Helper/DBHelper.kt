package com.example.dictionary.Databases.Helper

import android.app.Activity
import android.content.Context
import com.example.dictionary.API.Models.Dictionary
import com.example.dictionary.API.Models.Meaning
import com.example.dictionary.Adapters.ExpandableListViewAdapterDictionary
import com.example.dictionary.Adapters.ListAdapterDictionary
import com.example.dictionary.Databases.DictionaryDatabase
import com.example.dictionary.Util.log
import com.example.dictionary.Util.toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_offline_words.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class DBHelper(activity : Activity) {

    var activity=activity

    var dao= DictionaryDatabase.getInstance(activity.applicationContext).DictionaryDao()
        fun loadfromdb(word:String){

            GlobalScope.launch {
                activity.log("in loadword")
                var data =dao.getWord(word.lowercase())

                activity.runOnUiThread{
                    var d=if (data.size>=1) data[0] else null
                    if (d!=null) {
                        activity.wordres.setText(d.word)
                        activity.listview.adapter= ListAdapterDictionary(d.meanings as ArrayList<Meaning>)
                    }
                    else
                        activity.toast("Offline not available")
                }

            }

        }

        fun addword(res : Dictionary){
            // add word in lower case
            var dict=com.example.dictionary.Databases.Entity.Dictionary(res[0].word.lowercase(),res[0].meanings)


            GlobalScope.launch {
                dao.addWord(dict)
            }


        }


    fun loadallwords(){
        GlobalScope.launch {
            var words = dao.allwords()
            activity.runOnUiThread {
                if (words.size!=0) {
                    activity.exlistview.setAdapter(ExpandableListViewAdapterDictionary(words))
                }
            }
        }
    }

    fun deleteall(){
        GlobalScope.launch {
            dao.deleteall()
        }
        activity.runOnUiThread {
            activity.exlistview.setAdapter(ExpandableListViewAdapterDictionary(listOf<com.example.dictionary.Databases.Entity.Dictionary>()))
        }
    }





}