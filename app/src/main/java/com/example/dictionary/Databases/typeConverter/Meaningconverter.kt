package com.example.dictionary.Databases.typeConverter

import androidx.room.TypeConverter
import com.example.dictionary.API.Models.Meaning
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Meaningconverter {

    @TypeConverter
    fun fromMeaningList(value: List<Meaning>): String {
        val gson = Gson()
        val type = object : TypeToken<List<Meaning>>() {}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun toMeaningList(value: String): List<Meaning> {
        val gson = Gson()
        val type = object : TypeToken<List<Meaning>>() {}.type
        return gson.fromJson(value, type)
    }

}