package com.example.dictionary

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import com.example.dictionary.API.Models.Dictionary
import com.example.dictionary.API.Models.DictionaryItem
import com.example.dictionary.API.Models.Meaning
import com.example.dictionary.API.networkCalls.ApiCall
import com.example.dictionary.Adapters.ListAdapterDictionary
import com.example.dictionary.Databases.DictionaryDao
import com.example.dictionary.Databases.DictionaryDatabase
import com.example.dictionary.Databases.Helper.DBHelper
import com.example.dictionary.Util.log
import com.example.dictionary.Util.showpd
import com.example.dictionary.Util.toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        button.setOnClickListener{
            removeUIupdates()
            updateUI(editword.text.toString())
        }
    }

    private fun removeUIupdates() {
        wordres.setText("")
        listview.adapter=null
    }

    fun updateUI(word : String){
        var pd=showpd()
        ApiCall().getData(word).enqueue(object : Callback<Dictionary>{
            override fun onResponse(call: Call<Dictionary>, response: Response<Dictionary>) {
                if (!response.isSuccessful){
                    pd.dismiss()
                    toast("Not A Valid Word")
                    return
                }

                var res : Dictionary? = response.body()
                if (res is Dictionary){
                    if (res.size >=1){
                        wordres.setText(res[0].word)
                        listview.adapter=ListAdapterDictionary(res[0].meanings as ArrayList<Meaning>)
                        DBHelper(this@MainActivity).addword(res)
                    }

                    pd.dismiss()
                }
            }

            override fun onFailure(call: Call<Dictionary>, t: Throwable) {
                t.message?.let {
                    if (it.equals(ignoreCase = true, other = "Unable to resolve host \"api.dictionaryapi.dev\": No address associated with hostname")){
                        DBHelper(this@MainActivity).loadfromdb(word)
                    }
                    else
                        toast(it)
                }
                pd.dismiss()
            }

        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        MenuInflater(applicationContext).inflate(R.menu.menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        startActivity(Intent(this,OfflineWords :: class.java))
        return true
    }


}