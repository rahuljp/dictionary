package com.example.dictionary

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.dictionary.Databases.Helper.DBHelper
import kotlinx.android.synthetic.main.activity_offline_words.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class OfflineWords : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offline_words)
        supportActionBar?.title="Offline Words"
    }

    override fun onStart() {
        super.onStart()
        deleteall.setOnClickListener {
            DBHelper(this).deleteall()
        }
        DBHelper(this).loadallwords()
    }
}