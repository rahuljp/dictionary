package com.example.dictionary.Util

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.util.Log
import android.widget.Toast



fun Context.toast(text : String) {
    Toast.makeText(this,text,Toast.LENGTH_SHORT).show()
}

fun Activity.log(message : String){
    Log.d(this.localClassName,message)
}
fun Context.showpd() : ProgressDialog{
    var pd=ProgressDialog(this)
    pd.setCancelable(false)
    pd.setTitle("Loading")
    pd.setMessage("Please wait")
    pd.show()
    return pd
}

